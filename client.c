
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio_ext.h>
#include <ctype.h>

/* definizione di costanti */

#define SERVER_ADDR "127.0.0.1" 	// indirizzo server (locale)
#define SERVER_PORT 3490		 // porta server
#define BUFF_SIZE 1024 			// dimensione buffer
#define RW_SIZE 3 			// dimensione comando
#define COMMAND_SIZE 2			 // dimensione comando ricevuto dal client
#define FIELD_SIZE 20 			// dimensione campo

// messaggio di corretta autenticazione dell'utente
#define USER_PASS_OK "Utente autenticato correttamente" 

// messaggio di autenticazione fallita
#define RESEND "Username o password non corretti. Riprovare." 

// messaggio di tentativi di autenticazione esauriti
#define FAILED "Hai eseguito 3 tentativi di login senza successo. Riprova più tardi." 

// messaggio inviato al client in caso il server stia per disconnettersi
#define SERVER_DOWN "Server disconnesso"  

// messaggio inviato al client in caso di connessione accettata
#define CONNECTION_ACCEPTED "Connessione accettata" 

// messaggio inviato al client in caso di troppe connessioni contemporanee al server
#define CONNECTION_REFUSED "Connessione rifiutata" 

// la ricerca del contatto in elenco non ha avuto riscontri positivi
#define NO_RESULTS "Nessun contatto trovato" 

	/* definizione di variabili globali */
		int sockfd;

	/* dichiarazione di funzioni */
		void close_connection();
		void scrivi();
		void leggi();

/* definizione di funzioni */

//-------------- M A I N--------------

	int main(){

		int len; // lunghezza del messaggio

		// dichiarazione e allocazione buffer di ricezione
			char *buff;
			buff = malloc(BUFF_SIZE*sizeof(char));
			memset(buff, 0, BUFF_SIZE);

		// dichiarazione e allocazione buffer per le autorizzazioni
			char *rw;
			rw = malloc(RW_SIZE*sizeof(char));

		// flag per l'autenticazione
			char ok;
			ok = 0;

		// apertura socket
			sockfd = socket(AF_INET, SOCK_STREAM, 0); 
			if (sockfd == -1){ // eventuali erroi di apertura socket
				perror("Errore socket");
				exit(1);
			}

		struct sockaddr_in serv_addr; // conterrà l’indirizzo di destinazione
					     // settaggio attributi serv_addr
		
		// famiglia indirizzo
			serv_addr.sin_family = AF_INET; 
		// numero porta
			serv_addr.sin_port = htons(SERVER_PORT); 
		// indirizzo internet
			serv_addr.sin_addr.s_addr = inet_addr(SERVER_ADDR); 

		 // eguaglia la lunghezza della struttura a quella di struct sockaddr
			memset(serv_addr.sin_zero, 0, sizeof serv_addr.sin_zero);
	
		// connessione al server
			int conn = connect(sockfd, (struct sockaddr*)&serv_addr, sizeof serv_addr);
			if (conn == -1){
				perror("Errore connect");
				close(sockfd);
				exit(1);
			}

		// risposta del server per la connessione
			recv(sockfd, buff, BUFF_SIZE, 0);
			if (strcmp(buff, CONNECTION_REFUSED) == 0){
				printf("%s\n","Connessione rifiutata"); 
				close_connection();
				exit(1);
				}
			else 
				printf("%s\n","***Connesso***"); 

		do {	
			// azzeramento strutture di memorizzazione	
				memset(buff, 0, BUFF_SIZE);
				memset(rw, 0, RW_SIZE);

			// richiesta di autenticazione
				printf("%s\n","\nInserire username e password:");
			// acquisizione username e password da input
				fgets(buff, BUFF_SIZE, stdin); 

				len = strlen(buff);
				buff[len-1] = '\0'; // terminatore di stringa
		
			// invia dati al sever per la verifica delle credenziali di accesso
				send(sockfd, buff, len, 0);
		
			// risposta del server
				recv(sockfd, buff, BUFF_SIZE, 0);

				printf("Risposta del server: %s\n", buff);
			// caso server chiuso
				if (strcmp(buff, SERVER_DOWN) == 0){
					close_connection();
				}
				      // autenticazione avvenuta con successo
				else if (strcmp(buff, USER_PASS_OK) == 0){
						ok = 1;
						recv(sockfd, rw, RW_SIZE, 0);
						
						// stampa autorizzazioni
						if (strcmp(rw,"r-") == 0)
							printf("\n%s\n", "Sei autorizzato a leggere");
						else if (strcmp(rw,"-w") == 0)
								printf("\n%s\n", "Sei autorizzato a scrivere");
						     else if (strcmp(rw,"rw") == 0)
								printf("\n%s\n", "Sei autorizzato a leggere e scrivere");
					} 	
						// autenticazione non riuscita
					else if (strcmp(buff, FAILED) == 0) {
							printf("%s\n", "Client disconnesso");
							return(0);
						}
			} while (ok == 0);

	// loop principale
	//printf("%s\n", " l- LETTURA  s- SCRITTURA  c- CHIUSURA\n");
	while(1){
		printf("%s\n\n"," Cosa vuoi fare?\n\t\t l- LETTURA \n\t\t s- SCRITTURA\n \t\t c- CHIUSURA\n");
		
		memset(buff, 0, BUFF_SIZE);
		// acquisizione da standard input del comando da eseguire
			fgets(buff, 3, stdin); 
		
		// caso lettura
		if (strncmp(buff,"l",1) == 0){

			if(strcmp(rw,"r-") == 0 || strcmp(rw,"rw") == 0){
				printf("%s\n", "LETTURA"); 
				send(sockfd, buff, 1, 0);
				leggi();
			}
			else printf("%s\n", "### Non sei autorizzato a leggere ### ");
		}
		// caso scrittura
		else if (strncmp(buff,"s",1) == 0){

			if(strcmp(rw,"-w") == 0 || strcmp(rw,"rw") == 0){
				printf("%s\n", "SCRITTURA");
				send(sockfd, buff, 1, 0); 
				scrivi();
				recv(sockfd, buff, BUFF_SIZE, 0);
				printf("Risposta del server: %s\n", buff);
			}
			else printf("%s\n", "### Non sei autorizzato a scrivere ###\n");
		}
		// caso chiusura
		else if (strncmp(buff,"c",1) == 0){
			printf("%s\n", "CHIUSURA");
			return 0;
		} 
		// caso comando sbagliato
		else {
			printf("%s\n", "\nSono ammessi solo i comandi :\n\n\t = = = >   l- LETTURA  s- SCRITTURA  c- CHIUSURA   < = = =\n");
			//printf("%s\n", " \nl- LETTURA  s- SCRITTURA  c- CHIUSURA");
		   }
	} 

	// dellocazione strutture di memoria
	free(buff);
	free(rw);

	return 0;
}

// funzione di disconnessione del client
void close_connection(){
	if ((close(sockfd)) < 0) 
		perror("Errore: chiusura socket");
	printf("%s\n", "Client disconnesso");
	exit(0);
}

// funzione di invio dati al server per la scrittura
void scrivi(){

	int len; // lunghezza del messaggio
	int punt; // puntatore al carattere corrente
	int ok = 0; // 0 ok, 1 ko

	// campi dati
	char nome[FIELD_SIZE];
	char cognome[FIELD_SIZE];
	char numero[FIELD_SIZE];
	char* contatto; // buffer che conterrà i tre campi dati concatenati
	contatto = malloc(3*BUFF_SIZE); 
	
	// inizializzazione campi dati
	memset(nome, 0, FIELD_SIZE*sizeof(char));	
	memset(cognome, 0, FIELD_SIZE*sizeof(char));
	memset(numero, 0, FIELD_SIZE*sizeof(char));
	memset(contatto, 0, 3*FIELD_SIZE*sizeof(char)+2);

	// settaggio campi
	printf("%s\n","Inserisci nome:");
	__fpurge(stdin);
	fgets(nome, FIELD_SIZE, stdin);
	len = strlen(nome);
	punt = 0;

	//printf(" nome Inviato: %s\n", nome);			//stampa debug nome

	while(punt < len-1 && ok == 0){			
		//printf("Inviato: %c\n", nome[punt]);											
		if(isalpha(nome[punt]))
			 punt++;
		else 
			ok = 1; 
	}
	if(ok == 1){
		printf("%s\n","Il campo nome inserito non è valido in quanto contiene caratteri non letterali");
		close_connection();
	}
	nome[len-1] = '_';

	printf("%s\n","Inserisci cognome:");
	fgets(cognome, FIELD_SIZE, stdin);
	len = strlen(cognome); 
	punt = 0;			
	
	

	while(punt < len-1 && ok == 0){
		if(isalpha(cognome[punt]))
			punt++;
		else ok = 1;
	}
	if(ok == 1){
		printf("%s\n","Il campo cognome inserito non è valido in quanto contiene caratteri non letterali");
		close_connection();
	}
	cognome[len-1] = '_';

	printf("%s\n","Inserisci numero:");
	fgets(numero, FIELD_SIZE, stdin);
	len = strlen(numero);
	punt = 0;
	while(punt < len-1 && ok == 0){
		if(isdigit(numero[punt]))
			punt++ ;
		else ok = 1;
	}
	if(ok == 1){
		printf("%s\n","Il campo numero inserito non è valido in quanto contiene caratteri non numerici");
		close_connection();
	}
	numero[len-1] = '\0';

	// concatenazione campi
	strcat(nome, cognome);
	strcat(nome, numero);

	// copio il contenuto di nome in contatto
	memcpy(contatto, nome, strlen(nome)); 

	len = strlen(contatto);

	// invio contatto a server
	send(sockfd, contatto, len, 0);	
	printf("Inviato: %s\n", contatto);

	// deallocazione buffer
	free(contatto); 
}

// funzione di invio dati al server per la lettura
void leggi(){

	int len; // lunghezza del messaggio

	// campo dati e inizializzazione
	char buff[BUFF_SIZE];
	memset(buff, 0, BUFF_SIZE);
	
	// acquisizione da standard input della parola da cercare
	printf("%s\n","Inserisci parola da cercare:");
	__fpurge(stdin);
	fgets(buff, BUFF_SIZE, stdin);
	len = strlen(buff);
	buff[len-1] = '\0'; // terminatore di stringa

	// invio parola da cercare a sever
	send(sockfd, buff, len, 0);
	printf("Inviato: %s\n\n", buff);

	// reinizializzazione campo dati
	memset(buff, 0, BUFF_SIZE);

	// server non disponibile
	if (recv(sockfd, buff, BUFF_SIZE, 0) == 0)
		printf("%s\n", "Server non disponibile");

	//server disconnesso
	if (strcmp(buff, SERVER_DOWN) == 0){
		printf("%s\n", "Server disconnesso");
		close_connection();
	}

	// parsing e stampa dei risultati
	else {
		printf("%s\n", "Risultati:");
		if (strcmp(buff, NO_RESULTS) == 0)
			printf("\t%s\n", NO_RESULTS);
		else {
			char *end_str;
    		char *token = strtok_r(buff, " ", &end_str);
			while(token != NULL) {
				printf("\t");
				char *end_token;
				char *token2 = strtok_r(token, "_", &end_token);
				while(token2 != NULL) {
				    printf("%s ", token2);
				    token2 = strtok_r(NULL, "_", &end_token);
				}
				printf("\n");
				token = strtok_r(NULL, " ", &end_str);
			}
		}
	}
}
