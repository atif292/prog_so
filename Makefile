CC 		= gcc
CFLAGS 	= -Wall 

server: server.c client
	$(CC) $(CFLAGS) server.c -o server -pthread
	
client: client.c
	$(CC) $(CFLAGS) client.c -o client

.phony: clean

clean:
	rm -f client server elenco_telefonico.txt
