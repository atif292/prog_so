#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>

/* definizione di costanti */
#define MY_PORT 3490 			// numero di porta per le connessioni client
#define MY_ADDRESS "127.0.0.1" 		// ip address per le connessioni client
#define MAX_CONNECTIONS 10 		// massimo numero di connessioni in sospeso che può contenere la coda
#define MAX_USERS 10 			// massimo numero di utenti che il server può gestire contemporaneamente
#define BUFF_SIZE 100 			// dimensione buffer
#define RECORD_SIZE 1024 		// dimensione record
#define RW_SIZE 3 			// dimensione comando
#define COMMAND_SIZE 2 			// dimensione comando ricevuto dal client

// messaggio di corretta autenticazione dell'utente
#define USER_PASS_OK "Utente autenticato correttamente" 

// messaggio di autenticazione fallita
#define RESEND "Username o password non corretti. Riprovare." 

// messaggio di tentativi di autenticazione esauriti
#define FAILED "Hai eseguito 3 tentativi di login senza successo. Riprova più tardi." 

// messaggio inviato al client in caso il server stia per disconnettersi
#define SERVER_DOWN "Server disconnesso"  

// messaggio inviato al client in caso di connessione accettata
#define CONNECTION_ACCEPTED "Connessione accettata" 

// messaggio inviato al client in caso di troppe connessioni contemporanee al server
#define CONNECTION_REFUSED "Connessione rifiutata" 

// la ricerca del contatto in elenco non ha avuto riscontri positivi
#define NO_RESULTS "Nessun contatto trovato" 

	/* definizione di variabili globali */
		int sockfd; 			// socket in ascolto
		int sockets[MAX_USERS]; 	// array per memorizzare i socket dei client connessi
		FILE *fds[MAX_USERS]; 		// array per memorizzare i descrittori dei file che ogni utente aprirà in lettura/scrittura
		pthread_mutex_t lock; 		// definizione del lock
		int clients; 			// contatore dei client connessi

	/* dichiarazione di funzioni */
		void* start(void* arg);
		int login(int client_id);
		char* find_user(char* credenziali);
		int send_answer(int client_id, char* messaggio);
		void close_connection(int client_sock);
		int aggiungi(char *str);
		char *cerca_record(char *str);
		void close_server();

	/* definizione di funzioni */

// ------------------------- M A I N -------------------

	int main(){

		signal(SIGINT, close_server); // segnale di chiusura server

		// creazione del lock
			if (pthread_mutex_init(&lock, NULL) != 0){
				perror("Creazione del mutex fallita");
				exit(1);
   			 }

   		 // sblocco iniziale del lock
  			  pthread_mutex_unlock(&lock);

		// inizializzazione dell'array dei client connessi e dei loro descrittori dei file
			int i;
			for (i = 0; i < MAX_USERS; i++){
				sockets[i] = -1;
				fds[i] = NULL;
			}

		clients = 0; // contatore di client connessi posto inizialmente a 0

		int client_sock; 			// socket di connessione dei client col server
    		struct sockaddr_in my_addr;		// informazioni sull'indirizzo del server
		struct sockaddr_in their_addr; 		// informazioni sull’indirizzo di chi si connette
		int b, l;				 // valori di ritorno rispettivamente di bind() e listen()
		int sin_size; 				// utilizzato per memorizzare sizeof their_addr

		// creazione del socket di ascolto
			sockfd = socket(AF_INET, SOCK_STREAM, 0);
			if (sockfd == -1){
				perror("Errore socket");
				exit(1);
			}

		// settaggio attributi my_addr
			my_addr.sin_family = AF_INET; 				// famiglia indirizzo
			my_addr.sin_port = htons(MY_PORT); 			// numero porta
			my_addr.sin_addr.s_addr = inet_addr(MY_ADDRESS); 	// indirizzo internet
			memset(my_addr.sin_zero, 0, sizeof my_addr.sin_zero); 	// eguaglia la lunghezza della struttura a quella di struct sockaddr
	
		// permette di riusare l'indirizzo una volta chiuso il socket di ascolto
			int true = 1;
			setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int));
	
		// bind del socket
			b = bind(sockfd, (struct sockaddr*)&my_addr, sizeof my_addr);
			if (b == -1){
				perror("Errore bind");
				close(sockfd);
				exit(1);
			}
	
		// listen del socket
			l = listen(sockfd, MAX_CONNECTIONS);
			if (l == -1){
				perror("Errore listen");
				close(sockfd);
				exit(1);
			}

		// il server è in ascolto
		printf("Server in ascolto sulla porta %d...\n", MY_PORT);
	
		// loop principale
		while(1){

			// creazione di un nuovo socket per gestire una connessione in entrata
				sin_size = sizeof their_addr;
				client_sock = accept(sockfd, (struct sockaddr*)&their_addr, (socklen_t*)&sin_size);
				if (client_sock == -1){
					perror("Errore accept");
					close(sockfd);
					exit(1);
				}

			 

			/** inizio sezione critica **/

				pthread_mutex_lock(&lock);	 // settaggio lock bloccato
				clients++; 			// incremento contatore client connessi

				// caso numero di client connessi maggiore di quello consentito
				if (clients > MAX_USERS){
					send_answer(client_sock, CONNECTION_REFUSED); 
									// server informa client dell'impossibilità di instaurare la connessione
					close(client_sock);
					clients--; 			// decremento contatore client connessi
					pthread_mutex_unlock(&lock); 	// settaggio lock sbloccato
					continue; 			// salta all'iterazione successiva del ciclo
				}
				int c = 0;
				// inserimento socket nell'array delle connessioni accettate dal server 

				while (sockets[c] != -1 && c < MAX_USERS)	// scorre l'array dei socket fino a trovare un posto libero
					c++;

				sockets[c] = client_sock;
				pthread_mutex_unlock(&lock); 	// settaggio lock sbloccato

			/** fine sezione critica **/

			// dichiarazione del thread che gestirà la connessione
			pthread_t t;

			// preparazione del parametro da passare al nuovo thread
				int* arg = &client_sock;
		
			// creazione del thread		
			if (pthread_create(&t, NULL, start, arg) != 0){ // esegue la funzione start su un nuovo thread
				perror("Errore nella creazione del nuovo thread");
				exit(1);
			}
		}

		return 0;
	}

//--------------------------- F I N E  M A I N -------------------

// funzione eseguita in thread separato dal main
void* start(void* arg){
	
	int client_sock = *(int*)arg;		// socket per comunicare con il server
	int client_id = client_sock-3; 		// il 3 indica i 3 canali di I/O standard (stdin, stdout, stderr)
	char logged;

	// stampa id client connesso e conferma la connessione al client
	printf("***Client %d connesso***\n", client_id);
	send_answer(client_sock, CONNECTION_ACCEPTED);
	
	// chiamata alla funzione di login
	logged = login(client_sock);
	if (logged == 1){
		printf("***Client %d disconnesso***\n", client_id);
		pthread_exit(0);
	}
	
	char *res = malloc(RECORD_SIZE*sizeof(char)); 		// variabile per il risultato di cerca_record
	int bytes_recv; 					// numero di bytes ricevuti
	char command[COMMAND_SIZE]; 				// buffer per il comando da eseguire
	char buff[BUFF_SIZE]; 					// buffer per il messaggio ricevuto
	
	memset(res, 0, RECORD_SIZE);
	memset(command, 0, COMMAND_SIZE);

	while(1){
		// ricevi comando dal client
		bytes_recv = recv(client_sock, command, COMMAND_SIZE, 0);

		if(bytes_recv  > 0){
			memset(buff, 0, BUFF_SIZE);
			// caso lettura
			if (strcmp(command,"l") == 0){
				printf("Client %d: LETTURA\n", client_id);
				if((bytes_recv = recv(client_sock, buff, BUFF_SIZE, 0)) > 0){

					res = cerca_record(buff); 	// chiamata a funzione  cerca_record

					// contatto presente in elenco quindi mandato al client in risposta
					if (res != NULL){
						printf("Trovato: %s\n",res);
						send_answer(client_sock, res);
					}
					// contatto non presente
					else 
						send_answer(client_sock, NO_RESULTS);
				}	
				else {
					printf("***Client %d disconnesso***\n", client_id);
					close_connection(client_sock);
					free(res);
					pthread_exit(0);
				}
			}
			// caso scrittura
			else if(strcmp(command,"s") == 0){
				printf("Client %d: SCRITTURA\n", client_id);

				if((bytes_recv = recv(client_sock, buff, BUFF_SIZE, 0)) > 0){
					int presente = aggiungi(buff); 	// chiamata a funzione aggiungi

					// caso contatto già in elenco
					if (presente == -1){
						printf("%s\n", "Record già presente");
						send_answer(client_sock, "Record già presente");
					}
					else {
						printf("Aggiunto: %s\n", buff);
						send_answer(client_sock, "Record aggiunto");
					}
				}	
				else {
					printf("***Client %d disconnesso***\n", client_id);
					close_connection(client_sock);
					free(res);
					pthread_exit(0);
				}
			}
		}
		else {
			printf("***Client %d disconnesso***\n", client_id);
			close_connection(client_sock);
			free(res);
			pthread_exit(0);
		}	
	}
}


// funzione di login
int login(int client_id){
	
	int bytes_recv; 	// numero di bytes ricevuti
	char* buff;		 // buffer per il messaggio ricevuto
	
	char* rw; 		// autorizzazioni del client: r-, -w, rw
	char cont; 		// contatore tentativi di login

	cont = 0;
	rw = NULL;
	
	// allocazione buffer per ricevere le credenziali
	buff = malloc(BUFF_SIZE*sizeof(char));
	memset(buff, 0, BUFF_SIZE);

	do{
		// ricezione e gestione del messaggio
		bytes_recv = recv(client_id, buff, BUFF_SIZE, 0);

		if(bytes_recv > 0){
			rw = malloc(RW_SIZE*sizeof(char));
			memset(rw, 0, RW_SIZE);
			rw = find_user(buff);// invoca funzione find_user
		}
		else {
			printf("***Client %d disconnesso***\n", client_id-3);
			close_connection(client_id);
			pthread_exit(0);
		}
		// se non è stato autenticato l'utente, e si sono fatti meno di 3 tentativi, viene detto al client di riprovare
		if (rw == NULL && cont < 2) 
			send_answer(client_id, RESEND);
		cont++;
	} while (cont < 3 && rw == NULL);

	// deallocazione buffer per recv
	free(buff);

	// tentativi esauriti e utente non trovato
	if (rw == NULL){
		send_answer(client_id, FAILED);
		printf("%s\n", "Username e password non trovati");
		close_connection(client_id);
		free(rw);
		return 1;
	}
	// username e password corrispondenti ad un utente nel file di login
	else {
		send_answer(client_id, USER_PASS_OK);


		sleep(1); // serve per far smaltire il socket buffer 


		send_answer(client_id, rw);
		free(rw);
	}
	return 0;
}


// funzione di ricerca utente: restituisce le autorizzazioni o NULL se non è stato trovato
char* find_user(char* credenziali){	
	FILE *fd; 			// descrittore del file da cui leggere
	char buff[BUFF_SIZE]; 		// buffer che conterrà la stringa letta da file
	char *line; 			// puntatore alla stringa letta
	char trovato; 			// 1 se la ricerca è andata a buon fine, 0 altrimenti
	char *rw; 			// autorizzazioni del client: r-, -w, rw
	
	int credlen; // lunghezza della stringa inviata dal client che contiene username e password
	int linelen; // lunghezza della riga letta corrente
	
	rw = NULL;
	trovato = 0;
	credlen = strlen(credenziali); // lunghezza delle credenziali inserite dal client
	
	// apre il file in lettura
	fd = fopen("login.txt", "r+");
	if(fd == NULL) {
		perror("Errore nell'apertura del file");
		exit(1);
	}

	int c = 0;
	/** inizio sezione critica **/
	pthread_mutex_lock(&lock);
	// inserimento di fd nell'array dei descrittori di file 
	while (fds[c] != NULL && c < MAX_USERS)// scorre l'array dei descrittori di file fino a trovare un posto libero
		c++;
	fds[c] = fd;
	pthread_mutex_unlock(&lock);
	/** fine sezione critica **/
	
	line = fgets(buff, BUFF_SIZE, fd); // legge una riga dal file descritto da fd e la salva in buff

	// cicla finchè ci sono righe da leggere o finchè non trova una corrispondenza
	while(line != NULL && trovato == 0){ 
			
		linelen = strlen(line)-4; // lunghezza della riga letta da file (senza " rw")

		// confronta la lunghezza delle due stringhe e poi confronta le stringhe stesse
		if(credlen == linelen && strncmp(line, credenziali, linelen) == 0){ 
			rw = malloc(RW_SIZE*sizeof(char));
			strncpy(rw, line+linelen+1, RW_SIZE-1); // copia gli ultimi caratteri di line in rw
			rw[RW_SIZE-1] = '\0'; // aggiunge terminatore di stringa 
			trovato = 1;
		}
		line = fgets(buff, BUFF_SIZE, fd); // legge una riga dal file descritto da fd e la salva in buff
	}
	
	// chiude il file 
    fclose(fd);
	fds[c] = NULL;

	return rw;
} 


// funzione di invio sul socket client_id del messaggio mess
int send_answer(int client_id, char* mess){

	int bytes_sent, len;
	len = strlen(mess); // lunghezza messaggio
	
	// invio e gestione dell'invio del messaggio
	bytes_sent = send(client_id, mess, len, 0);
	if (bytes_sent == -1){
		perror("Errore send");
		exit(1);
	}
	else if (bytes_sent < len){
		perror("Inviati meno bytes del previsto");
		exit(1);
	}
	else {
		printf("Inviato al client %d: %s\n", client_id-3, mess);// stampa imessaggio inviato sul terminale
	}
	return 0;
}


// funzione per la scrittura su file di nuovi utenti dell'elenco telefonico	
int aggiungi(char *str){

	// cerca se record già presente
	char *res = cerca_record(str);// chiamata alla funzione cerca_record
	if (res != NULL){
		free(res);
		return -1;
	}

	FILE *fd; 
	// apre il file in scrittura
  	fd = fopen("elenco_telefonico.txt", "a");
  	if( fd==NULL ) {
    	perror("Errore in apertura del file");
    	exit(1);
  	}

  	int c = 0;
  	/** inizio sezione critica **/
  	pthread_mutex_lock(&lock);
  	// inserimento di fd nell'array dei descrittori di file 
	while (fds[c] != NULL && c < MAX_USERS)// scorre l'array dei descrittori di file fino a trovare un posto libero
		c++;
	fds[c] = fd;
	
  	fprintf(fd, "%s\n", str);// scrive il nuovo utente nel file
  	pthread_mutex_unlock(&lock);
	/** fine sezione critica **/

	// chiude il file
  	fclose(fd);
  	fds[c] = NULL;

  	return 0;
}
	

// funzione di ricerca utente nell'elenco telefonico	
char *cerca_record(char *str){

	// sostituisco ogni spazio nella stringa con '_'
	int i=0;
    while(str[i] != '\0'){
        if(str[i] == ' ')
            str[i] = '_';
        i++; 
    }

	printf("Stringa da cercare: %s\n", str);
	FILE *fd; 
	char *res = NULL;

	// dichiara e inizializza buffer e record
	char buff[BUFF_SIZE];
	char temp[RECORD_SIZE];
	memset(buff, 0, BUFF_SIZE);
	memset(temp, 0, RECORD_SIZE);
	
	// apertura file elenco telefonico
	fd = fopen("elenco_telefonico.txt", "r"); 
	if(fd == NULL) {
    	perror("Errore in apertura del file");
    	exit(1);
 	}

  	int c = 0;

  	/** inizio sezione critica **/
  	pthread_mutex_lock(&lock);
  	// inserimento di fd nell'array dei descrittori di file 
	while (fds[c] != NULL && c < MAX_USERS)  // scorre l'array dei descrittori di file fino a trovare un posto libero
		c++;
	fds[c] = fd;
	pthread_mutex_unlock(&lock);
	/** fine sezione critica **/

	// cerca fino al raggiungimento della fine del file righe uguali a quella passata
	while (!feof(fd)){
		fscanf(fd, "%s\n", buff);
		if (strstr(buff, str) != NULL){
			strcat(temp, buff);
			strcat(temp, " "); // serve per il parsing lato client
		}
	}
	if (strcmp(temp, "\0") != 0){
		res = malloc(RECORD_SIZE*sizeof(char));
		strcpy(res, temp);
	}

	// chiude il file
	fclose(fd); 
	fds[c] = NULL;

	return res;
}


// chiude il server
void close_server(){ 
	printf("%s\n", " Closing...");

	int i;
	for (i = 0; i < MAX_USERS; i++){
		printf("sockets[%d]: %d\n", i, sockets[i]); // stampa l'array dei socket per mostrare quali sono connessi e quali no 
		// invia messaggio di server non disponibile ad ogni socket di client connesso, poi lo chiude
		if (sockets[i] != -1){
			send_answer(sockets[i], SERVER_DOWN);
			close(sockets[i]);
			clients--; // decrementa il numero di client connessi
		}
	}
	// chiude il socket di ascolto
	close(sockfd);
	printf("%s\n", "Server closed");
    exit(0);
}


// chiude la connessione con un client
void close_connection(int client_sock){
	int c = 0;
	/** inizio sezione critica **/
	pthread_mutex_lock(&lock);
	// rimuove il socket del client dall'array dei socket
	while (sockets[c] != client_sock)
		c++;
	sockets[c] = -1;
	pthread_mutex_unlock(&lock);
	clients--;
	/** fine sezione critica **/
	close(client_sock);
}
